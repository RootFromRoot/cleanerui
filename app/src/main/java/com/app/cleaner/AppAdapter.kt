package com.app.cleaner

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_app.view.*

class AppAdapter : RecyclerView.Adapter<AppViewHolder>() {

    private var items: ArrayList<App> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AppViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_app, parent, false)
    )

    override fun getItemCount() = items.size

    fun setApps(reply: ArrayList<App>) {
        this.items = reply
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        holder.apply {
            name.text = items[position].name
            desc.text = items[position].desc
            totalSpace.text = items[position].totalSpace
            cacheSize.text = items[position].cacheSize
            path.text = items[position].path

            appIcon.setBackgroundResource(R.drawable.ic_google)

            expandButton.setOnCheckedChangeListener { button, isChecked ->
                if (isChecked) wrapperInfo.visibility = View.VISIBLE
                else wrapperInfo.visibility = View.GONE
            }
        }
    }

}

class AppViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var name = view.tv_app_name!!
    var desc = view.tv_app_description!!
    var appIcon = view.ic_app_icon!!
    var totalSpace = view.tv_total_space_value!!
    var cacheSize = view.tv_cache_size!!
    var path = view.tv_path!!
    var expandButton = view.chn_expand_google!!
    var wrapperInfo = view.wrapper_google_info!!
}