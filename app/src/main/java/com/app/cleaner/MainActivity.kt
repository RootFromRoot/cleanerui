package com.app.cleaner

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_advanced_cleaning.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
    }

    private fun initialize() {
        setContentView(R.layout.activity_main)
        nav_view.selectedItemId = R.id.navigation_disc
        v_line_disk.visibility = View.VISIBLE

        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, MainFragment.newInstance(), "main_fragment")
            .commit()

        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_cpu -> {
                    tv_toolbar_tittle.text = getString(R.string.cpu_usage)
                    v_line_cpu.visibility = View.VISIBLE
                    v_line_disk.visibility = View.INVISIBLE
                    v_line_ram.visibility = View.INVISIBLE
                    v_line_battery.visibility = View.INVISIBLE
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_disc -> {
                    tv_toolbar_tittle.text = getString(R.string.disk_usage)
                    v_line_cpu.visibility = View.INVISIBLE
                    v_line_disk.visibility = View.VISIBLE
                    v_line_ram.visibility = View.INVISIBLE
                    v_line_battery.visibility = View.INVISIBLE
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_ram -> {
                    tv_toolbar_tittle.text = getString(R.string.ram_usage)
                    v_line_cpu.visibility = View.INVISIBLE
                    v_line_disk.visibility = View.INVISIBLE
                    v_line_ram.visibility = View.VISIBLE
                    v_line_battery.visibility = View.INVISIBLE
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_battery -> {
                    tv_toolbar_tittle.text = getString(R.string.battery)
                    v_line_cpu.visibility = View.INVISIBLE
                    v_line_disk.visibility = View.INVISIBLE
                    v_line_ram.visibility = View.INVISIBLE
                    v_line_battery.visibility = View.VISIBLE
                    startActivity(Intent(this, AdvancedCleaningActivity::class.java))
                    return@setOnNavigationItemSelectedListener true
                }
            }
            false
        }
    }
}
