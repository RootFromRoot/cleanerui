package com.app.cleaner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment: Fragment(){
    companion object {

        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onResume() {
        super.onResume()
        progressbar_used_from.setProgress(80)
        progressbar_used_from.showProgressText(false)
        progressbar_used_from.setProgressColor(resources.getColor(R.color.color_red))

        progress_total_space.setProgress(70)
        progress_total_space.showProgressText(false)
        progress_total_space.setProgressColor(resources.getColor(R.color.colorPrimaryDark))

        progress_filled.setProgress(100)
        progress_filled.showProgressText(false)
        progress_filled.setProgressColor(resources.getColor(R.color.color_gray))
    }

}