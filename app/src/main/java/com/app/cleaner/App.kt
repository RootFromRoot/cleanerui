package com.app.cleaner

data class App(
    var name: String,
    var desc: String,
    var totalSpace: String,
    var cacheSize: String,
    var path: String
)