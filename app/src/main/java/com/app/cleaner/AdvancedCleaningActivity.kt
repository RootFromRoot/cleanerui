package com.app.cleaner

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_advanced_cleaning.*

class AdvancedCleaningActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
    }

    private fun initialize() {
        setContentView(R.layout.activity_advanced_cleaning)
        setupRv()
        iv_back.setOnClickListener {
            finish()
        }
    }

    fun setupRv() {

        val testApp: ArrayList<App> = ArrayList()

        testApp.add(App("Google", "Google inl", "5.11", "239 KB", "testPath"))
        testApp.add(App("Google", "Google inl", "5.11", "239 KB", "testPath"))
        testApp.add(App("Google", "Google inl", "5.11", "239 KB", "testPath"))


        val layoutManager = LinearLayoutManager(this)
        val adapter = AppAdapter()
        rv_app_list.layoutManager = layoutManager
        rv_app_list.adapter = adapter

        adapter.setApps(testApp)

    }
}
